#include "gtest/gtest.h"
#include "gurobi_c++.h"

#include <fstream>


struct GurobiTestLinear : testing::Test {

    GRBEnv env = GRBEnv();
    GRBModel model = GRBModel(env);
    GRBVar x = model.addVar(0.0, 1.0, 0.0, GRB_CONTINUOUS, "x");
    GRBVar y = model.addVar(0.0, 1.0, 0.0, GRB_BINARY, "y");
    GRBVar z = model.addVar(0.0, 1.0, 0.0, GRB_BINARY, "z");
    GurobiTestLinear() {
        model.setObjective(x + y + 2 * z, GRB_MAXIMIZE);
        model.addConstr(x + 2 * y + 3 * z <= 4, "c0");
        model.addConstr(x + y >= 1, "c1");
        model.set(GRB_IntParam_OutputFlag, 0);
    }

};


struct GurobiTestQuadratic : testing::Test {

    GRBEnv env = GRBEnv();
    GRBModel model = GRBModel(env);
    GRBVar x = model.addVar(0.0, 1.0, 0.0, GRB_CONTINUOUS, "x");
    GRBVar y = model.addVar(0.0, 1.0, 0.0, GRB_CONTINUOUS, "y");
    GRBVar z = model.addVar(0.0, 1.0, 0.0, GRB_CONTINUOUS, "z");
    GurobiTestQuadratic() {
        GRBQuadExpr obj = x * x + x * y + y * y + y * z + z * z + 2 * x;
        model.setObjective(obj, GRB_MINIMIZE);
        model.addQConstr(x + 2 * y + 3 * z >= 4, "c0");
        model.addConstr(x + y >= 1, "c1");
        model.set(GRB_IntParam_OutputFlag, 0);
    }

};



//testing initialization of environment and (empty) model
//mainly to check if a valid license is available
TEST(GurobiTestInitialization, EnvironmentAndModel) {

    const std::string failureNotice = "\n"
                                      "    **************************************************************************************\n"
                                      "    **************************************************************************************\n"
                                      "    * NOTE: This will likely cause all other tests related to Gurobi to fail as well!\n"
                                      "    **************************************************************************************\n"
                                      "    **************************************************************************************\n"
                                      "\n";

    try {
        GRBEnv env = GRBEnv();
    }
    catch(const GRBException& e) {
        FAIL() << "Encountered GRBException while initializing Gurobi environment:\n"
               << "    " << e.getMessage() << "\n"
               << failureNotice;
    }
    catch(...) {
        FAIL() << "Encountered unkown exception while initializing Gurobi environment.\n"
               << failureNotice;
    }

    try {
        GRBEnv env = GRBEnv();
        GRBModel model = GRBModel(env);
    }
    catch(const GRBException& e) {
        FAIL() << "Encountered GRBException while initializing an empty Gurobi model:\n"
               << "    " << e.getMessage() << "\n"
               << failureNotice;
    }
    catch(...) {
        FAIL() << "Encountered unkown exception while initializing an empty Gurobi model.\n"
               << failureNotice;
    }

}


//testing write()
TEST_F(GurobiTestLinear, WriteFile) {

    std::remove("test.lp");
    ASSERT_NO_THROW(model.write("test.lp"));
    std::ifstream file("test.lp");
    ASSERT_TRUE(file.is_open());
    file.close();
    std::remove("test.lp");

}


//testing the correctness of optimize() and if the model resets
TEST_F(GurobiTestLinear, CorrectRes) {

    model.optimize();
    ASSERT_EQ(1, x.get(GRB_DoubleAttr_X));
    ASSERT_EQ(0, y.get(GRB_DoubleAttr_X));
    ASSERT_EQ(1, z.get(GRB_DoubleAttr_X));
    model.reset();
    model.update();
    ASSERT_ANY_THROW(z.get(GRB_DoubleAttr_X));

}


//testing correctness of  optimize() for quadratic problems and the precision of the solver and the attribibute FeasibilityTol
TEST_F(GurobiTestQuadratic, CorrectRes) {

    x.set(GRB_CharAttr_VType, GRB_INTEGER);
    model.optimize();
    ASSERT_EQ(0, x.get(GRB_DoubleAttr_X));
    ASSERT_EQ(1, y.get(GRB_DoubleAttr_X));
    ASSERT_NEAR((2.0/3.0), z.get(GRB_DoubleAttr_X), GRB_DoubleParam_FeasibilityTol);

}


//testing if all the parameters/ attributes concerning Integer variables used in MAiNGO are still available
TEST_F(GurobiTestLinear, IntParametersAvail) {

    ASSERT_NO_THROW(model.get(GRB_IntParam_BarIterLimit));
    ASSERT_NO_THROW(model.get(GRB_IntParam_Method));
    ASSERT_NO_THROW(model.get(GRB_IntParam_OutputFlag));
    ASSERT_NO_THROW(model.get(GRB_IntParam_Presolve));
    ASSERT_NO_THROW(model.get(GRB_IntAttr_Status));

}


// //testing if all the parameters/attributes concerning double variables used in MAiNGO are still available
TEST_F(GurobiTestLinear, DoubleParametersAvail) {

    ASSERT_NO_THROW(model.get(GRB_DoubleParam_BarConvTol));
    ASSERT_NO_THROW(model.get(GRB_DoubleParam_BarQCPConvTol));
    ASSERT_NO_THROW(model.get(GRB_DoubleParam_IterationLimit));
    ASSERT_NO_THROW(x.set(GRB_DoubleAttr_LB, 2));
    ASSERT_NO_THROW(model.get(GRB_DoubleParam_NodeLimit));
    ASSERT_NO_THROW(model.get(GRB_DoubleParam_OptimalityTol));
    ASSERT_NO_THROW(x.set(GRB_DoubleAttr_UB, 2));

}


//testing the attributes concerning constraints, Pi (shadow price) is only available for continuous linear problems
TEST_F(GurobiTestLinear, ConstraintParams) {

    x.set(GRB_CharAttr_VType, GRB_CONTINUOUS);
    y.set(GRB_CharAttr_VType, GRB_CONTINUOUS);
    z.set(GRB_CharAttr_VType, GRB_CONTINUOUS);
    GRBConstr testConstr = model.addConstr(x + y <= 6, "ct");
    model.optimize();
    ASSERT_NEAR(6., testConstr.get(GRB_DoubleAttr_RHS), GRB_DoubleParam_FeasibilityTol);
    ASSERT_NEAR(0., testConstr.get(GRB_DoubleAttr_Pi), GRB_DoubleParam_FeasibilityTol);

}


//testing the FarkasDual Attribute
TEST(GurobiInfeasibilityTest, FarkasDualTest) {

    GRBEnv env = GRBEnv();
    GRBModel model = GRBModel(env);
    model.set(GRB_IntParam_OutputFlag, 0);
    ASSERT_NO_THROW(model.set(GRB_IntParam_InfUnbdInfo, 1));
    GRBVar x = model.addVar(0.0, 1.0, 0.0, GRB_CONTINUOUS, "x");
    GRBVar y = model.addVar(0.0, 1.0, 0.0, GRB_CONTINUOUS, "y");
    model.setObjective(-x + y, GRB_MAXIMIZE);
    GRBConstr testConstr = model.addConstr(-x + y >= 1, "ct");
    GRBConstr testConstr2 = model.addConstr(x - y >= 1, "ct2");
    model.optimize();
    ASSERT_NEAR(-1., testConstr.get(GRB_DoubleAttr_FarkasDual), GRB_DoubleParam_FeasibilityTol);

}


//testing if changing a coefficient is changing the result of the optimization and the attribute ObjVal
TEST_F(GurobiTestLinear, ChangeCoefficient) {

    model.optimize();
    GRBConstr testConstr =  model.addConstr(x <= 1, "ct");
    const double before = x.get(GRB_DoubleAttr_X);
    model.chgCoeff(testConstr, x, 2);
    model.optimize();
    ASSERT_NE(before, x.get(GRB_DoubleAttr_X));
    ASSERT_NO_THROW(model.get(GRB_DoubleAttr_ObjVal));

}
